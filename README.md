# StratioPhoneBill
This project will calculate the phone bill of a client based on their call logs.

## Prerequisites
* [Java 1.8 JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [SBT 1.2.6](https://www.scala-sbt.org/download.html)
* [Scala 2.12.7](https://www.scala-lang.org/download/)

### Optional (but recommended)
* [IntelliJ IDEA](https://www.jetbrains.com/idea/download)

## Installation
On this steps I will be using IntelliJ IDEA. Installation in Eclipse should be similar.
1. Import the project in IntelliJ IDEA (Best IDE for Scala development. Far better than Eclipse).

![Importing](https://i.imgur.com/siNNqcX.png)


## Running
Once the project is loaded (and compiled, automatically done by IntelliJ IDEA) you can run it. To do so, just right-clock on `StratioPhoneBillTest.scala` file under the `src/test/scala` directory and select `Run 'StratioPhoneBillTest'`.

The execution should look like this:

![Running](https://i.imgur.com/VWxFnTO.png)

