import scala.util.matching.Regex

object StratioPhoneBill {
  private lazy val LINE_SEPARATOR: Char = '\n'
  private lazy val COST_PER_SECOND: Int = 3 // 3 cents per second if the call duration is under 5 minutes
  private lazy val COST_PER_MINUTE: Int = 150 // 150 cents per minute when 5 minutes has passed
  private lazy val MAX_MINUTES: Int = 5

  /**
    * Calculates the client's total bill charge analyzing their phone call logs
    *
    * @param log Phone call logs
    * @return Total amount to be charged to the client
    */
  def calculateBillingCharge(log: String): Int = {
    // Split the log by the Line Separator Character (\n)
    val calls = log.split(LINE_SEPARATOR).map(parseLogLine)

    // Group each call with their durations and sum them up
    val grouped = calls.groupBy(_.phoneNumber).map {
      case (phoneNumber: String, calls: Array[PhoneCall]) => PhoneCall(phoneNumber, calls.map(_.totalSeconds).sum, calls.map(_.cost).sum)
    }.toList

    // Order the phone calls by their total duration
    val ordered = grouped.sortBy(_.totalSeconds)

    // Get the most used phone number and drop it of the list. It's free!
    val free = ordered.last
    println(s"Calls to this number are free! ${free.phoneNumber}")

    // Drop the most used phone number cause its free! Then sum it all
    val totalCost = ordered.filter( entry => !entry.phoneNumber.equals(free.phoneNumber))
                    .map(_.cost)
                    .sum
    println(s"Total cost of the bill: $totalCost")

    totalCost
  }


  /**
    * Parses a log line from the phone call log of an user to an object
    *
    * @param line Line to be parsed
    * @return Parsed line
    */
  private def parseLogLine(line: String): PhoneCall = {
    val regex: Regex = "(\\d{2}):(\\d{2}):(\\d{2}),(\\d{3}-\\d{3}-\\d{3})".r

    line match {
      case regex(hours, minutes, seconds, phoneNumber) => {
        val totalSeconds: Int = Integer.parseInt(hours) * 60 * 60 + Integer.parseInt(minutes) * 60 + Integer.parseInt(seconds)
        val cost = calculateCallCost(totalSeconds)
        println(s"Phone: $phoneNumber, Total Seconds: $totalSeconds, Cost: $cost")

        PhoneCall(phoneNumber, totalSeconds, cost)
      }
      case _ => throw new IllegalArgumentException(s"Bad formatted log line: $line")
    }
  }

  /**
    * Calculates the cost of a single call based on the seconds spent on the phone
    *
    * @param seconds Duration of the call in seconds
    * @return Price to charge to the customer
    */
  private def calculateCallCost(seconds: Int): Int = {
    val minutes = Math.ceil(seconds / 60.0).toInt

    if (minutes >= MAX_MINUTES)
      minutes * COST_PER_MINUTE
    else
      seconds * COST_PER_SECOND
  }

  private case class PhoneCall(phoneNumber: String, totalSeconds: Int, cost: Int)

}
