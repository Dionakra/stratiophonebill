import org.scalatest.{FlatSpec, Matchers}

class StratioPhoneBillTest extends FlatSpec with Matchers {

  "The example phone bill" should "charge 900 to the client" in {
    val log = "00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090"

    val billing = StratioPhoneBill.calculateBillingCharge(log)
    assert(billing === 900)
  }
}
